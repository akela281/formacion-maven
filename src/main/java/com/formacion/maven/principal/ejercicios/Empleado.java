package com.formacion.maven.principal.ejercicios;

public class Empleado {
	private String identificador;
	private String area;
	private Double salario;
	
	public String getIdentificador() {
		return identificador;
	}
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public Double getSalario() {
		return salario;
	}
	public void setSalario(Double salario) {
		this.salario = salario;
	}
	
	public Double executeCalculateSalary(Double Salario, int edad) {
		Double salarioCalculado = 0.0;
		if (edad < 16) {
			salarioCalculado = Salario;
			System.out.println("no tiene edad para trabajar");
		} 
		if (edad > 16 && edad < 50) {
			salarioCalculado = (Salario * 1.05);
			System.out.println("el salario es un 5 por ciento m�s " + salarioCalculado);
		} 
		if (edad > 51 && edad < 60) {
			salarioCalculado = (Salario * 1.10);
			System.out.println("el salario es un 10 por ciento m�s "+ salarioCalculado);
		} 
		if (edad > 60 ) {
			salarioCalculado = (Salario * 1.15);
			System.out.println("el salario es un 15 por ciento m�s " + salarioCalculado);
		} 
		return salarioCalculado;
	}
	@Override
	public String toString() {
		return "Empleado [identificador=" + identificador + ", area=" + area + ", salario=" + salario + "]";
	}
	

}
