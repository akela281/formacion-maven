package com.formacion.maven.principal.ejercicios;

import java.util.Scanner;

public class SumaNumero {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
           Scanner sc = new Scanner(System.in);
           int num, sumPar = 0, sumNon = 0;
           //ingresa numero
           System.out.println("introduce numero: " );
           num = sc.nextInt();
           for (int i = 1; i <= num; i++) { 
        	   if (i % 2 == 0) {
        		   sumPar = sumPar + i;
        		
        	   }else {
        		   sumNon = sumNon + i ;
        	   }
           }
           System.out.println("Suma de pares es: " + sumPar);
           System.out.println("Suma de nones es: " + sumNon);
   	}

}
