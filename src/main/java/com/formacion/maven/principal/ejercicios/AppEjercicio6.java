package com.formacion.maven.principal.ejercicios;

import java.util.Scanner;

public class AppEjercicio6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner capturar = new Scanner(System.in);
		System.out.print("Introduce empleados a capturar: ");
		int j = Integer.parseInt(capturar.next());
		
		String empleados[] = new String[j];
		for (int i = 0; i < empleados.length; i++) {
			Persona empleado = new Persona();
			System.out.print("Introduce nombre: ");
			empleados[i] = capturar.next();
			empleado.setNombre(empleados[i]);

			System.out.print("Introduce edad: ");
			empleados[i] = capturar.next();
			int edadEntera = Integer.parseInt(empleados[i]);
			empleado.setEdad(edadEntera);

			System.out.print("Introduce genero: ");
			empleados[i] = capturar.next();
			empleado.setGenero(empleados[i]);

			System.out.print("Introduce salario: ");
			empleados[i] = capturar.next();
			Double sueldoEntero = Double.parseDouble(empleados[i]);
			empleado.setSalario(sueldoEntero);

			System.out.println(empleado.toString());

			empleado.executeCalculateSalary(empleado.getSalario(), empleado.getEdad());

		}

	}
}
