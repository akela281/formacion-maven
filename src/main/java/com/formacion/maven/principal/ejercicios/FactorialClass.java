package com.formacion.maven.principal.ejercicios;

import java.util.Scanner;

public class FactorialClass {
	public static void main(String args[]) {
		int num = 0;
		long result = 1;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introduzca un n�mero: ");
		num = sc.nextInt();
		for (int i = 1; i <= num; i++) {
			result *= i;
		}
		
		System.out.println("El resultado es: " + result);
	}
}
