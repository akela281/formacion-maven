package com.formacion.maven.principal.ejercicios;

public class Mathematics {
	public static void suma(int num1, int num2) {
		System.out.println("Resultado: " + (num1 + num2)); 
	}
	
	public static void resta(int num1, int num2) {
		System.out.println("Resultado: " + (num1 - num2)); 
	}
	
	public static void multiplicacion(int num1, int num2) {
		System.out.println("Resultado: " + (num1 * num2)); 
	}
	
	public static void dividir(int num1, int num2) {
		System.out.println("Resultado: " + (num1 / num2)); 
		}
}
