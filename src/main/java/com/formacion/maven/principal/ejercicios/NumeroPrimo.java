package com.formacion.maven.principal.ejercicios;

import java.util.Scanner;

public class NumeroPrimo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num;
		boolean otro;
		Scanner sc = new Scanner(System.in);
		
		do {

			System.out.print("Introduce un n�mero entero: ");
			num = sc.nextInt();
		
			if (num < 2) {
				System.out.println("El numero NO es primo");
			}else {
				if (esPrimo(num)) {
					System.out.println("El numero es primo");
				}else {
					System.out.println("El numero NO es primo");
				}
			}
			
			System.out.print("Desea introducir otro numero? (true/false) ");
			otro = sc.nextBoolean();
		
		} while (otro);
		
		if (!otro) {
			System.out.println("FIN");
		}
	}
	
	public static boolean esPrimo (int num) {
		int cont, i;
		cont = 0;
		i = 0;
		for (i = 2; i <= num; i++) {
			if(num % i == 0 ) {
				cont++;
			}
		}
		
		if (cont >= 2) {
			return false;
		}else {
			return true;
		}
		
	}
}

