package com.formacion.maven.principal.ejercicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AppEjercicio10 {
	
	static List<Persona>  empleados = new ArrayList<>();
	static String bandera;
	static Scanner capturar = new Scanner(System.in);
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//String bandera;
		//Scanner capturar = new Scanner(System.in);
		//List<Persona>  empleados = new ArrayList<>();
		ingresaEmpleado();
		
		imprimir();
		if (empleados.size() == 7 ) {
			empleados.remove(7);
			System.out.print("Elimina empleado en posicion 7: ");
			imprimir();
		} 
		
		System.out.print("Ingresar otro empleado Y/N: ");
		bandera = capturar.next();
		if (bandera.equals("Y")||bandera.equals("y")) {
			ingresaEmpleado();
			imprimir();
			
		}
		
			
		 
	}
	
	static void ingresaEmpleado(){
		String bandera;
		//Scanner capturar = new Scanner(System.in);
		//List<Persona>  empleados = new ArrayList<>();
		do {
			Persona empleado = new Persona();
			System.out.print("Introduce nombre: ");
			empleado.setNombre(capturar.next());  
			
			System.out.print("Introduce edad: ");
			empleado.setEdad(Integer.parseInt(capturar.next()));
			

			System.out.print("Introduce genero: ");
			empleado.setGenero(capturar.next());

			System.out.print("Introduce salario: ");
			empleado.setSalario(Double.parseDouble(capturar.next()));
			
			
			empleados.add(empleado);

			System.out.println(empleado.toString());

			empleado.executeCalculateSalary(empleado.getSalario(), empleado.getEdad());
			
			System.out.print("Se ingresa otro empleado Y/N: ");
			
			bandera = capturar.next();
			
		} 
		
		while (bandera.equals("Y")||bandera.equals("y") && empleados.size() < 8 );
		
		
	}
	static void imprimir() {
		for (Persona item : empleados) {
			System.out.println("salario " + item.getNombre()+ " " + item.getSalario());
		 	}
		}

}
