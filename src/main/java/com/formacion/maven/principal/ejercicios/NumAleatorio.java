package com.formacion.maven.principal.ejercicios;

public class NumAleatorio {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int numA, numB, numC, i = 0;
		boolean combinacion;
		
		combinacion = false;
		while (!combinacion) {
			numA = (int) (Math.random() * 1000);
			numB = (int) (Math.random() * 1000);
			numC = (int) (Math.random() * 1000);
		
			if (numA % 2 == 0) {
				if (numB % 2 == 0) {
					if (numC % 2 != 0) {
						i++;
						combinacion = true;
						System.out.println("*****************************************************");
						System.out.println("1er n�mero: " + numA);
						System.out.println("2o  n�mero: " + numB);
						System.out.println("3er n�mero: " + numC);
						System.out.println("La combinaci�n Par, Par, Impar se obtuvo en " + i + " ensayos.");
					}else {
						i++;
						System.out.println("Ensayo " + i +", n�meros: " + numA + ", " + numB +", " + numC);
					}
				}else {
					i++;
					System.out.println("Ensayo " + i +", n�meros: " + numA + ", " + numB +", " + numC);
				}
			}else {
				i++;
				System.out.println("Ensayo " + i +", n�meros: " + numA + ", " + numB +", " + numC);
			}
		}	
		
	}

}
